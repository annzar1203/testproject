const path = require('path');

const webpack = require('webpack');


const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    entry: ['babel-regenerator-runtime', './src/js/index.js'],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index-bundle.js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [{ loader: 'url-loader' }],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            filename: 'index.html',
            hash: true,
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    resolve: {
        modules: [path.join(__dirname, 'src', 'js'), 'node_modules'],
        extensions: ['.js', '.jsx', '.scss'],
    },
};
