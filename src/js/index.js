import React from 'react';
import ReactDOM from 'react-dom';

import App from './main/components/App.js';

import './style.scss';

ReactDOM.render(<App />, document.getElementById('root'));
