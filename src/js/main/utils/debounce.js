export function debounce(callback, time) {
    let timer = null;

    const start = (...args) => {
        if (timer) {
            clearTimeout(timer);
        }
        const onComplete = () => {
            callback.apply(this, ...args);
            timer = null;
        };
        timer = setTimeout(onComplete, time);
    };
    return start;
}
