import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { hideModal } from '../actions/modalActions';
import { searchUsers } from '../actions/mainActions';
import { Main } from '../components/MainPage/MainPageRecompose';


const mapStateToProps = ({ users, modal }) => ({
    users: users.get('data'),
    isPending: users.get('isPending'),
    isActive: modal.get('isActive'),
    modalProps: modal.get('modalProps'),
    message: users.get('message'),
});

const mapDispatchToProps = dispatch => ({
    onSearchUsers: bindActionCreators(searchUsers, dispatch),
    onClose: bindActionCreators(hideModal, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Main);
