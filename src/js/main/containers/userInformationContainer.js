import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import UserInformation from '../components/UserInformation/UserInformation';

import { getUser, getRepos } from '../actions/mainActions';

const mapStateToProps = ({ user, repos }) => ({
    user: user.get('data'),
    repos: repos.get('data'),
    isLoading: !user.get('isPending') && !repos.get('isPending'),
});

const mapDispatchToProps = dispatch => ({
    fetchUser: bindActionCreators(getUser, dispatch),
    fetchRepo: bindActionCreators(getRepos, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserInformation);
