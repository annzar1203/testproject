import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { showModal } from '../../actions/modalActions';

import UserTableItem from '../../components/Table/TableRows/UserTableItem';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
    showUserInformation: bindActionCreators(showModal, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserTableItem);
