import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ReposTableItem from '../../components/Table/TableRows/ReposTableItem';

import { getIssues } from '../../actions/mainActions';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
    getIssuesAction: bindActionCreators(getIssues, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ReposTableItem);
