import { Record } from 'immutable';

export class UserRecord extends Record({
    login: null,
    avatar_url: null,
}) {
    static parse(obj) {
        return new UserRecord({
            login: obj.login,
            avatar_url: obj.avatar_url,
        });
    }
}
