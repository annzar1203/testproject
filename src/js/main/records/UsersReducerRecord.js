import { Record, List } from 'immutable';
import { UserRecord } from './UserRecord';

export class UsersReducerRecord extends Record({
    data: new List(),
    isPending: false,
    message: null,
}) {
    static parse(data) {
        return new UsersReducerRecord({
            data: data ? new List(data.map(i => UserRecord.parse(i))) : new List(),
        });
    }
}
