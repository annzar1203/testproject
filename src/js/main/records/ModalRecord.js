import { Record } from 'immutable';

export class ModalRecord extends Record({
    isActive: false,
    modalProps: null,
}) {}
