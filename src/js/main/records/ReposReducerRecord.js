import { Record, List } from 'immutable';
import { RepoRecord } from './RepoRecord';

export class ReposReducerRecord extends Record({
    data: new List(),
    isPending: true,
    message: null,
}) {
    static parse(data) {
        return new ReposReducerRecord({
            data: data ? new List(data.map(i => RepoRecord.parse(i))) : null,
        });
    }
}
