import { Record } from 'immutable';

export class RepoRecord extends Record({
    name: null,
    description: null,
    issues: null,
    isPending: true,
}) {
    static parse(obj) {
        return new RepoRecord({
            name: obj.name,
            description: obj.description || null,
            issues: obj.issues || null,
            isPending: (obj.issues && false) || true,
        });
    }
}
