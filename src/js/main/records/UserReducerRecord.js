import { Record } from 'immutable';
import { UserRecord } from './UserRecord';

export class UserReducerRecord extends Record({
    data: new UserRecord(),
    isPending: null,
    message: null,
}) {
    static parse(data) {
        return new UserReducerRecord({
            data: data ? UserRecord.parse(data) : new UserRecord(),
        });
    }
}
