import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { RepoRecord } from '../../../records/RepoRecord';

import { TABLE_MESSAGE, headersLabels } from '../../../constants/tableConstants';
import '../table.scss';


export default class ReposTableItem extends PureComponent {
    static propTypes = {
        item: PropTypes.instanceOf(RepoRecord).isRequired,
        headers: PropTypes.arrayOf(
            PropTypes.shape({
                key: PropTypes.string,
                label: PropTypes.string,
            }),
        ).isRequired,
        username: PropTypes.string.isRequired,
        getIssuesAction: PropTypes.func.isRequired,
    };

    componentDidMount() {
        const { getIssuesAction, username, item } = this.props;

        getIssuesAction(username, item.get('name'));
    }

    renderItemElement = (header, index) => {
        const { item } = this.props;
        switch (header.label) {
        case headersLabels.DESCRIPTION:
            return (
                <td className="table-td" key={index}>
                    {item.get(header.key) || TABLE_MESSAGE}
                </td>
            );
        case headersLabels.ISSUES:
            return (
                <td className="table-td" key={index}>
                    {!item.get('isPending') ? item.get(header.key) : <div className="circle-small" />}
                </td>
            );
        default:
            return (
                <td className="table-td" key={index}>
                    {item.get(header.key)}
                </td>
            );
        }
    };

    render() {
        const { headers } = this.props;

        return <tr className="tableItem table-tr">{headers.map(this.renderItemElement)}</tr>;
    }
}
