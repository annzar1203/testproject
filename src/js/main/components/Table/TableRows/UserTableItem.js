import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { UserRecord } from '../../../records/UserRecord';

import '../table.scss';

export default class UserTableItem extends PureComponent {
    static propTypes = {
        item: PropTypes.instanceOf(UserRecord).isRequired,
        headers: PropTypes.arrayOf(
            PropTypes.shape({
                key: PropTypes.string,
                label: PropTypes.string,
            }),
        ).isRequired,
        showUserInformation: PropTypes.func.isRequired,
    };

    renderItemElement = (header, index) => {
        const { item } = this.props;

        return (
            <td className="table-td" key={index}>
                {header.label === 'Avatar' ? (
                    <img className="table-img" src={item.get(header.key)} alt=" " />
                ) : (
                    item.get(header.key)
                )}
            </td>
        );
    };

    onRowClick = () => {
        const { showUserInformation, item } = this.props;

        showUserInformation(item.get('login'));
    };

    render() {
        const { headers } = this.props;

        return (
            <tr className="tableItem table-tr" onClick={this.onRowClick}>
                {headers.map(this.renderItemElement)}
            </tr>
        );
    }
}
