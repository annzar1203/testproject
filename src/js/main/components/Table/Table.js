import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'immutable-prop-types';

import UserTableItem from '../../containers/tableContainers/userTableContainers';

import './table.scss';


const defaultRow = (item, index, headers) => <UserTableItem item={item} key={index} headers={headers} />;

const defaultHeaders = [{ key: 'avatar_url', label: 'Avatar' }, { key: 'login', label: 'Name' }];

export default class Table extends PureComponent {
    static propTypes = {
        headers: PropTypes.arrayOf(PropTypes.object),
        data: ImmutablePropTypes.list.isRequired,
        renderRow: PropTypes.func,
    };

    static defaultProps = {
        headers: defaultHeaders,
        renderRow: defaultRow,
    };

    renderItem = (item, index) => {
        const { renderRow, headers } = this.props;
        return renderRow(item, index, headers);
    };

    render() {
        const { headers, data } = this.props;

        return (
            <table className="table">
                {headers.length && (
                    <thead>
                        <tr className="table-tr">
                            {headers.map((header, index) => (
                                <th className="table-th" key={index}>
                                    {header.label}
                                </th>
                            ))}
                        </tr>
                    </thead>
                )}
                <tbody className="table-tbody">{data.map(this.renderItem)}</tbody>
            </table>
        );
    }
}
