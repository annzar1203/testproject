import React, { PureComponent } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'immutable-prop-types';

import { headersLabels } from '../../constants/tableConstants';
import { UserRecord } from '../../records/UserRecord';

import Table from '../Table/Table';
import ReposTableItem from '../../containers/tableContainers/reposTableContainer';

import '../Modal/scss/style.scss';
import '../MainPage/scss/style.scss';
import '../Table/table.scss';

const headers = [
    { key: 'name', label: headersLabels.REPO_NAME },
    { key: 'description', label: headersLabels.DESCRIPTION },
    { key: 'issues', label: headersLabels.ISSUES },
];

export default class UserInformation extends PureComponent {
    static propTypes = {
        username: PropTypes.string.isRequired,
        isLoading: PropTypes.bool.isRequired,

        fetchUser: PropTypes.func.isRequired,
        user: PropTypes.instanceOf(UserRecord).isRequired,

        fetchRepo: PropTypes.func.isRequired,
        repos: ImmutablePropTypes.list.isRequired,
        onClose: PropTypes.func.isRequired,
    };

    componentDidMount() {
        const { fetchUser, fetchRepo, username } = this.props;

        fetchUser(username);
        fetchRepo(username);
    }

    renderRow = (item, index) => {
        const { user } = this.props;

        return <ReposTableItem item={item} key={index} headers={headers} username={user.get('login')} />;
    };

    renderInformation = () => {
        const { user, repos, onClose } = this.props;

        return (
            <div>
                {user && (
                    <React.Fragment>
                        <div className="flex-content-space-between">
                            <h2>{`User: ${user.get('login')}`}</h2>
                            <button type="button" className="close button" onClick={onClose}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="user-information">
                            <img className="table-img" src={user.get('avatar_url')} alt=" " />
                            {repos && repos.count() ? (
                                <div className="modal-table">
                                    <Scrollbars autoHeight autoHeightMax={270}>
                                        <Table data={repos} headers={headers} renderRow={this.renderRow} />
                                    </Scrollbars>
                                </div>
                            ) : (
                                <h3>Repositories not found</h3>
                            )}
                        </div>
                    </React.Fragment>
                )}
            </div>
        );
    };

    render() {
        const { isLoading } = this.props;

        return isLoading ? (
            this.renderInformation()
        ) : (
            <div className="flex-box">
                <div className="circle" />
            </div>
        );
    }
}
