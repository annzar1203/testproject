import React from 'react';
import { Provider } from 'react-redux';

import MainPage from '../containers/mainPageContainer';
import createStore from '../store/index';

const store = createStore();

export default () => (
    <Provider store={store}>
        <MainPage />
    </Provider>
);
