import React, { PureComponent } from 'react';
import * as ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import './scss/style.scss';
import '../MainPage/scss/style.scss';

export default class Modal extends PureComponent {
    static propTypes = {
        children: PropTypes.element.isRequired,
    };

    componentDidMount() {
        document.body.style.overflow = 'hidden';
    }

    componentWillUnmount() {
        document.body.style.overflow = 'auto';
    }

    render() {
        const { children } = this.props;

        return ReactDOM.createPortal(
            <div className="modal flex">{children}</div>,
            document.getElementById('modal-portal'),
        );
    }
}
