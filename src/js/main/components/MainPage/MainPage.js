import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'immutable-prop-types';

import { UserRecord } from 'main/records/UserRecord';
import Table from '../Table/Table';
import Modal from '../Modal/Modal';
import UserInformation from '../../containers/userInformationContainer';

import './scss/style.scss';
import '../Modal/scss/style.scss';
import { debounce } from '../../utils/debounce';

export default class MainPage extends PureComponent {
    static propTypes = {
        onSearchUsers: PropTypes.func.isRequired,
        isPending: PropTypes.bool,
        users: ImmutablePropTypes.listOf(PropTypes.instanceOf(UserRecord)).isRequired,
        message: PropTypes.string,

        isActive: PropTypes.bool,
        onClose: PropTypes.func,
        modalProps: PropTypes.shape({
            username: PropTypes.string,
        }),
    };

    static defaultProps = {
        onClose: () => {},
        isActive: false,
        isPending: false,
        message: '',
        modalProps: { username: '' },
    };

    state = {
        searchField: '',
    };

    onChangeInput = (event) => {
        this.setState({ searchField: event.target.value });
        debounce(this.onSearchUser, 700)();
    };

    onHandleKeyDown = (event) => {
        if (event.keyCode === 27) {
            this.setState({ searchField: '' });
        }
        if (event.keyCode === 13) {
            event.preventDefault();
            this.onSearchUser();
        }
    };

    onSearchUser = () => {
        const { onSearchUsers } = this.props;
        const { searchField } = this.state;

        if (searchField) {
            onSearchUsers(searchField);
        }
    };

    render() {
        const {
            isPending, users, message, onClose, isActive, modalProps,
        } = this.props;
        const { searchField } = this.state;

        return (
            <React.Fragment>
                <div className="flex-box">
                    <h1>Github Browser</h1>
                    <div className="container">
                        <form className="form-flex-box">
                            <h2>Enter github user name</h2>
                            <input
                                placeholder="Enter github user name"
                                value={searchField}
                                onChange={this.onChangeInput}
                                onKeyDown={this.onHandleKeyDown}
                            />
                            <div>
                                {isPending && (
                                    <div className="flex-box">
                                        <div className="circle-medium" />
                                    </div>
                                )}
                            </div>
                        </form>
                    </div>
                    {users.count() && (
                        <div className="container">
                            <Table data={users} />
                        </div>
                    )}
                    {message && (
                        <div className="container">
                            <h2>{message}</h2>
                        </div>
                    )}
                </div>
                {isActive && (
                    <React.Fragment>
                        <Modal onClose={onClose}>
                            <UserInformation username={modalProps.username} onClose={onClose} />
                        </Modal>
                        <div className="modal-fade" />
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}
