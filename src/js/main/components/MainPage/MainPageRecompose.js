import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'immutable-prop-types';
import { compose, withState, withHandlers } from 'recompose';

import { UserRecord } from 'main/records/UserRecord';
import Table from '../Table/Table';
import Modal from '../Modal/Modal';
import UserInformation from '../../containers/userInformationContainer';

import './scss/style.scss';
import '../Modal/scss/style.scss';
import { debounce } from '../../utils/debounce';

const onSearch = compose(
    withState('searchField', 'onSetField', ' '),
    withHandlers({
        onSearchUser: props => () => {
            const { onSearchUsers, searchField } = props;

            if (searchField) {
                onSearchUsers(searchField);
            }
        },
    }),
    withHandlers({
        onHandleKeyDown: props => (e) => {
            if (e.keyCode === 27) {
                const { onSetField } = props;

                onSetField('');
            }
            if (e.keyCode === 13) {
                e.preventDefault();
                const { onSearchUser } = props;

                onSearchUser();
            }
        },
        onChangeInput: props => (e) => {
            const { onSetField, onSearchUser } = props;
            onSetField(e.target.value);
            debounce(onSearchUser, 700)();
        },
    }),
);

export const MainPageRecompose = (props) => {
    const {
        isPending,
        users,
        message,
        onClose,
        isActive,
        modalProps,
        searchField,
        onChangeInput,
        onHandleKeyDown,
    } = props;

    return (
        <React.Fragment>
            <div className="flex-box">
                <h1>Github Browser</h1>
                <div className="container">
                    <form className="form-flex-box">
                        <h2>Enter github user name</h2>
                        <input
                            placeholder="Enter github user name"
                            value={searchField}
                            onChange={onChangeInput}
                            onKeyDown={onHandleKeyDown}
                        />
                        <div>
                            {isPending && (
                                <div className="flex-box">
                                    <div className="circle-medium" />
                                </div>
                            )}
                        </div>
                    </form>
                </div>
                {users.count() && (
                    <div className="container">
                        <Table data={users} />
                    </div>
                )}
                {message && (
                    <div className="container">
                        <h2>{message}</h2>
                    </div>
                )}
            </div>
            {isActive && (
                <React.Fragment>
                    <Modal onClose={onClose}>
                        <UserInformation username={modalProps.username} onClose={onClose} />
                    </Modal>
                    <div className="modal-fade" />
                </React.Fragment>
            )}
        </React.Fragment>
    );
};

MainPageRecompose.propTypes = {
    onSearchUsers: PropTypes.func.isRequired,
    searchField: PropTypes.string,
    onChangeInput: PropTypes.func.isRequired,
    onHandleKeyDown: PropTypes.func.isRequired,
    isPending: PropTypes.bool,
    users: ImmutablePropTypes.listOf(PropTypes.instanceOf(UserRecord)).isRequired,
    message: PropTypes.string,

    isActive: PropTypes.bool,
    onClose: PropTypes.func,
    modalProps: PropTypes.shape({
        username: PropTypes.string,
    }),
};

MainPageRecompose.defaultProps = {
    onClose: () => {},
    isActive: false,
    isPending: false,
    message: '',
    modalProps: { username: '' },
    searchField: '',
};

export const Main = compose(onSearch)(MainPageRecompose);
