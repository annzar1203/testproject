import {
    GET_USER_SUCCESS,
    GET_REPO_SUCCESS,
    SEARCH_USER_SUCCESS,
    GET_ISSUES_SUCCESS,
    GET_USER_REQUEST,
    GET_REPO_REQUEST,
    SEARCH_USER_REQUEST,
    GET_ISSUES_REQUEST,
    GET_USER_ERROR,
    GET_REPO_ERROR,
    SEARCH_USER_ERROR,
    GET_ISSUES_ERROR,
} from '../constants/actionTypeConstants';
import { controllers } from '../controllers/axiosApiController';

export function getUserSuccess(data) {
    return {
        type: GET_USER_SUCCESS,
        payload: { data },
    };
}

export function getUserRequest() {
    return {
        type: GET_USER_REQUEST,
    };
}

export function getUserError() {
    return {
        type: GET_USER_ERROR,
    };
}

export function getReposSuccess(data) {
    return {
        type: GET_REPO_SUCCESS,
        payload: { data },
    };
}

export function getReposRequest() {
    return {
        type: GET_REPO_REQUEST,
    };
}

export function getReposError() {
    return {
        type: GET_REPO_ERROR,
    };
}

export function searchUsersSuccess(data, message) {
    return {
        type: SEARCH_USER_SUCCESS,
        payload: { data, message },
    };
}

export function searchUsersRequest() {
    return {
        type: SEARCH_USER_REQUEST,
    };
}

export function searchUsersError() {
    return {
        type: SEARCH_USER_ERROR,
    };
}

export function getIssuesSuccess(data) {
    return {
        type: GET_ISSUES_SUCCESS,
        payload: { data },
    };
}

export function getIssuesRequest() {
    return {
        type: GET_ISSUES_REQUEST,
    };
}

export function getIssuesError() {
    return {
        type: GET_ISSUES_ERROR,
    };
}

export function getUser(user) {
    return async (dispatch) => {
        dispatch(getUserRequest());

        try {
            const data = await controllers.getUser(user);
            dispatch(getUserSuccess(data));
        } catch (err) {
            dispatch(getUserError());
        }
    };
}

export function getRepos(user) {
    return async (dispatch) => {
        dispatch(getReposRequest());

        try {
            const data = await controllers.getRepos(user);
            dispatch(getReposSuccess(data));
        } catch (err) {
            dispatch(getReposError());
        }
    };
}

export function searchUsers(user) {
    return async (dispatch) => {
        dispatch(searchUsersRequest());
        try {
            const data = await controllers.searchUser(user);
            const message = !data.count() ? `User ${user} not found` : '';
            dispatch(searchUsersSuccess(data, message));
        } catch (err) {
            dispatch(searchUsersError());
        }
    };
}

export function getIssues(user, repo) {
    return async (dispatch, getState) => {
        dispatch(getIssuesRequest());
        try {
            const issues = await controllers.getIssues(user, repo);
            const data = getState().repos.get('data');
            const index = data.findIndex(item => item.name === repo);
            const updatedData = data.set(
                index,
                data
                    .get(index)
                    .set('issues', issues.length)
                    .set('isPending', false),
            );

            dispatch(getIssuesSuccess(updatedData));
        } catch (err) {
            dispatch(getIssuesError());
        }
    };
}
