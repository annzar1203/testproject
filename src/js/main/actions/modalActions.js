import {
    DELETE_REPO_INFORMATION,
    DELETE_USER_INFORMATION,
    SHOW_MODAL,
    HIDE_MODAL,
} from '../constants/actionTypeConstants';

export function hideModal() {
    return (dispatch) => {
        dispatch({
            type: `${DELETE_REPO_INFORMATION}`,
        });
        dispatch({
            type: `${DELETE_USER_INFORMATION}`,
        });
        dispatch({
            type: `${HIDE_MODAL}`,
        });
    };
}

export function showModal(username) {
    return {
        type: `${SHOW_MODAL}`,
        payload: {
            isActive: true,
            modalProps: {
                username,
            },
        },
    };
}
