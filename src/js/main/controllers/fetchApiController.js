import { List } from 'immutable';

import { UserRecord } from 'main/records/UserRecord';
import { RepoRecord } from 'main/records/RepoRecord';
import { API } from 'main/constants/actionTypeConstants';


export async function fetchUser(user) {
    const url = `${API}users/${user}`;
    const res = await fetch(url, {
        method: 'GET',
        headers: {
            'User-Agent': 'request',
            Authorization: 'token 6c5af4ee49471a51a9032c3f6ef6165ade821608',
        },
    });
    let data = await res.json();
    data = UserRecord.parse(data);
    return data;
}

export async function fetchSearchUser(user) {
    const url = `${API}search/users?q=${user}`;
    const res = await fetch(url, {
        method: 'GET',
        headers: {
            'User-Agent': 'request',
            Authorization: 'token 6c5af4ee49471a51a9032c3f6ef6165ade821608',
        },
    });
    let data = await res.json();
    data = new List(data.items.map(UserRecord.parse));
    return data;
}

export async function fetchGetRepos(user) {
    const url = `${API}users/${user}/repos`;
    const res = await fetch(url, {
        method: 'GET',
        headers: {
            'User-Agent': 'request',
            Authorization: 'token 6c5af4ee49471a51a9032c3f6ef6165ade821608',
        },
    });
    const data = await res.json();

    return new List(data.map(RepoRecord.parse));
}

export async function fetchGetIssues(user, repo) {
    const url = `${API}repos/${user}/${repo}/issues`;
    const res = await fetch(url, {
        method: 'GET',
        headers: {
            'User-Agent': 'request',
            Authorization: 'token 6c5af4ee49471a51a9032c3f6ef6165ade821608',
        },
    });
    const data = await res.json();

    return data;
}
