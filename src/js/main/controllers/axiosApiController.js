import axios from 'axios';
import { List } from 'immutable';

import { API } from '../constants/requestConstants';

import { UserRecord } from '../records/UserRecord';
import { RepoRecord } from '../records/RepoRecord';

axios.defaults.baseURL = API;
axios.defaults.headers.common.Authorization = 'token 6c5af4ee49471a51a9032c3f6ef6165ade821608';

async function getUser(user) {
    return axios.get(`/users/${user}`).then((res) => {
        const data = UserRecord.parse(res.data);
        return data;
    });
}

async function searchUser(user) {
    return axios.get(`search/users?q=${user}`).then((res) => {
        const data = new List(res.data.items.map(UserRecord.parse));
        return data;
    });
}

async function getRepos(user) {
    return axios.get(`users/${user}/repos`).then((res) => {
        const data = new List(res.data.map(RepoRecord.parse));
        return data;
    });
}

async function getIssues(user, repo) {
    return axios.get(`repos/${user}/${repo}/issues`).then((res) => {
        const { data } = res;

        return data;
    });
}

export const controllers = {
    getUser,
    searchUser,
    getRepos,
    getIssues,
};
