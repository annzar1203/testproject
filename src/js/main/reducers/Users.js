import { List } from 'immutable';

import { SEARCH_USER_REQUEST, SEARCH_USER_SUCCESS, SEARCH_USER_ERROR } from 'main/constants/actionTypeConstants';
import { ERROR_MESSAGE } from '../constants/requestConstants';

import { UsersReducerRecord } from '../records/UsersReducerRecord';

export default function UsersReducer(state = new UsersReducerRecord(), action) {
    switch (action.type) {
    case SEARCH_USER_REQUEST:
        return state.set('isPending', true);
    case SEARCH_USER_SUCCESS:
        return state
            .set('isPending', false)
            .set('data', action.payload.data)
            .set('message', action.payload.message);
    case SEARCH_USER_ERROR:
        return state
            .set('isPending', false)
            .set('data', new List())
            .set('message', ERROR_MESSAGE);
    default:
        return state;
    }
}
