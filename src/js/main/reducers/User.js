import {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_ERROR,
    DELETE_USER_INFORMATION,
} from '../constants/actionTypeConstants';

import { UserRecord } from '../records/UserRecord';
import { UserReducerRecord } from '../records/UserReducerRecord';

import { ERROR_MESSAGE } from '../constants/requestConstants';

export default function UserReducer(state = new UserReducerRecord(), action) {
    switch (action.type) {
    case GET_USER_REQUEST:
        return state.set('isPending', true);
    case GET_USER_SUCCESS:
        return state.set('isPending', false).set('data', action.payload.data);
    case GET_USER_ERROR:
        return state
            .set('isPending', false)
            .set('data', new UserRecord())
            .set('message', ERROR_MESSAGE);
    case DELETE_USER_INFORMATION:
        return state.set('isPending', true).set('data', new UserRecord());
    default:
        return state;
    }
}
