import { combineReducers } from 'redux';
import usersReducer from './Users';
import userReducer from './User';
import modalReducer from './Modal';
import reposReducer from './Repos';

export default combineReducers({
    users: usersReducer,
    modal: modalReducer,
    user: userReducer,
    repos: reposReducer,
});
