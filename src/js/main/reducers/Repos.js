import { List } from 'immutable';
import {
    GET_REPO_REQUEST,
    GET_REPO_SUCCESS,
    GET_REPO_ERROR,
    GET_ISSUES_REQUEST,
    GET_ISSUES_SUCCESS,
    GET_ISSUES_ERROR,
    DELETE_REPO_INFORMATION,
} from 'main/constants/actionTypeConstants';
import { ERROR_MESSAGE } from '../constants/requestConstants';

import { ReposReducerRecord } from '../records/ReposReducerRecord';

export default function ReposReducer(state = new ReposReducerRecord(), action) {
    switch (action.type) {
    case GET_REPO_REQUEST:
        return state.set('isPending', true);
    case GET_REPO_SUCCESS:
        return state.set('isPending', false).set('data', action.payload.data);
    case GET_REPO_ERROR:
        return state
            .set('isPending', false)
            .set('data', new List())
            .set('message', ERROR_MESSAGE);
    case GET_ISSUES_REQUEST:
        return state.set('isPending', false);
    case GET_ISSUES_SUCCESS:
        return state.set('isPending', false).set('data', action.payload.data);
    case GET_ISSUES_ERROR:
        return state.set('isPending', false);
    case DELETE_REPO_INFORMATION:
        return state.set('isPending', true).set('data', new List());
    default:
        return state;
    }
}
