import { ModalRecord } from '../records/ModalRecord';

import { SHOW_MODAL, HIDE_MODAL } from '../constants/actionTypeConstants';


export default function ModalReducer(state = new ModalRecord(), action) {
    switch (action.type) {
    case SHOW_MODAL:
        return state.set('isActive', action.payload.isActive).set('modalProps', action.payload.modalProps);
    case HIDE_MODAL:
        return state.set('isActive', false);
    default:
        return state;
    }
}
