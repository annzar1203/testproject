import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { GET_USER_REQUEST, GET_USER_SUCCESS, GET_USER_ERROR } from '../../js/main/constants/actionTypeConstants';

import {
    getUser, getUserSuccess, getUserRequest, getUserError,
} from '../../js/main/actions/mainActions';
import { UserRecord } from '../../js/main/records/UserRecord';

export const mockStore = configureMockStore([thunk]);

describe('getUser', () => {
    test('returns the correct action type SUCCESS', async () => {
        const store = mockStore();
        await store.dispatch(getUser('test'));
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: GET_USER_REQUEST });
        expect(actions[1]).toEqual({
            type: GET_USER_SUCCESS,
            payload: {
                data: new UserRecord({
                    login: 'test',
                    avatar_url: 'https://avatars3.githubusercontent.com/u/383316?v=4',
                }),
            },
        });
    });
});

describe('getUser', () => {
    test('returns the correct action type ERROR', async () => {
        const store = mockStore();
        await store.dispatch(getUser('test_test'));
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: GET_USER_REQUEST });
        expect(actions[1]).toEqual({ type: GET_USER_ERROR });
    });
});

describe('getUserRequest', () => {
    test('Dispatches the correct action and payload', () => {
        const store = mockStore();
        const expectedActions = [
            {
                type: GET_USER_REQUEST,
            },
        ];
        store.dispatch(getUserRequest());
        expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('getUserError', () => {
    test('Dispatches the correct action and payload', () => {
        const store = mockStore();
        const expectedActions = [
            {
                type: GET_USER_ERROR,
            },
        ];
        store.dispatch(getUserError());
        expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('getUserSuccess', () => {
    test('Dispatches the correct action and payload', () => {
        const store = mockStore();
        const expectedActions = [
            {
                type: GET_USER_SUCCESS,
                payload: {
                    data: 'test',
                },
            },
        ];
        store.dispatch(getUserSuccess('test'));
        expect(store.getActions()).toEqual(expectedActions);
    });
});
