import { ERROR_MESSAGE } from '../../js/main/constants/requestConstants';
import {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_ERROR,
    DELETE_USER_INFORMATION,
} from '../../js/main/constants/actionTypeConstants';

import { UserReducerRecord } from '../../js/main/records/UserReducerRecord';
import { UserRecord } from '../../js/main/records/UserRecord';

import userReducer from '../../js/main/reducers/User';

describe('GET_USER_REQUEST', () => {
    test('returns the correct state', () => {
        const action = { type: GET_USER_REQUEST };
        const expectedState = new UserReducerRecord({ isPending: true });
        expect(userReducer(undefined, action)).toEqual(expectedState);
    });
});

describe('GET_USER_SUCCESS', () => {
    test('returns the correct state', () => {
        const action = {
            type: GET_USER_SUCCESS,
            payload: {
                data: new UserRecord({
                    login: 'test',
                    avatar_url: 'test',
                }),
            },
        };
        const expectedState = new UserReducerRecord({
            isPending: false,
            data: new UserRecord({
                login: 'test',
                avatar_url: 'test',
            }),
        });
        expect(userReducer(undefined, action)).toEqual(expectedState);
    });
});

describe('GET_USER_ERROR', () => {
    test('returns the correct state', () => {
        const action = { type: GET_USER_ERROR };
        const expectedState = new UserReducerRecord({
            isPending: false,
            data: new UserRecord(),
            message: ERROR_MESSAGE,
        });
        expect(userReducer(undefined, action)).toEqual(expectedState);
    });
});

describe('DELETE_USER_INFORMATION', () => {
    test('returns the correct state', () => {
        const action = { type: DELETE_USER_INFORMATION };
        const expectedState = new UserReducerRecord({
            isPending: true,
            data: new UserRecord(),
        });
        expect(userReducer(undefined, action)).toEqual(expectedState);
    });
});

describe('DEFAULT_USER_STATE', () => {
    test('returns the correct state', () => {
        const action = { type: 'TEST' };
        const expectedState = new UserReducerRecord();
        expect(userReducer(undefined, action)).toEqual(expectedState);
    });
});
