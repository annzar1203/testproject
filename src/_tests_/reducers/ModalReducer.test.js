import { ModalRecord } from '../../js/main/records/ModalRecord';
import Modal from '../../js/main/reducers/Modal';
import { HIDE_MODAL, SHOW_MODAL } from '../../js/main/constants/actionTypeConstants';

describe('HIDE_MODAL', () => {
    test('returns the correct state', () => {
        const action = { type: HIDE_MODAL };
        const expectedState = new ModalRecord({ isActive: false, modalProps: null });

        expect(Modal(undefined, action)).toEqual(expectedState);
    });
});

describe('SHOW_MODAL', () => {
    test('returns the correct state', () => {
        const action = {
            type: SHOW_MODAL,
            payload: {
                isActive: true,
                modalProps: {
                    username: '1',
                },
            },
        };
        const expectedState = new ModalRecord({
            isActive: true,
            modalProps: {
                username: '1',
            },
        });

        expect(Modal(undefined, action)).toEqual(expectedState);
    });
});

describe('DEFAULT_MODAL_STATE', () => {
    test('returns the correct state', () => {
        const action = { type: 'TEST' };
        const expectedState = new ModalRecord();

        expect(Modal(undefined, action)).toEqual(expectedState);
    });
});
