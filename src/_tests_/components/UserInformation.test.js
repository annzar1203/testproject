import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import { List } from 'immutable';

import { RepoRecord } from '../../js/main/records/RepoRecord';
import { UserRecord } from '../../js/main/records/UserRecord';
import UserInformation from '../../js/main/components/UserInformation/UserInformation';

Enzyme.configure({ adapter: new Adapter() });

describe('UserInformation', () => {
    const baseProps = {
        onClose: () => {},
        fetchRepo: () => {},
        fetchUser: () => {},
        repos: new List(),
        isLoading: true,
        user: new UserRecord({
            login: 'test',
            avatar_url: 'test',
        }),
        username: 'test',
    };

    test('successfully calls the onClose handler', () => {
        const mockOnClick = jest.fn();
        const wrapper = shallow(<UserInformation {...baseProps} onClose={mockOnClick} />);

        wrapper.find('.button').simulate('click');
        expect(mockOnClick).toBeCalled();
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('successfully calls FetchUser', () => {
        const mockOnCallFetchUser = jest.fn();
        const wrapper = shallow(<UserInformation {...baseProps} fetchUser={mockOnCallFetchUser} />);

        expect(mockOnCallFetchUser).toBeCalledWith('test');
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('successfully calls FetchRepo', () => {
        const mockOnCallFetchRepo = jest.fn();
        const wrapper = shallow(<UserInformation {...baseProps} fetchRepo={mockOnCallFetchRepo} />);

        expect(mockOnCallFetchRepo).toBeCalledWith('test');
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('successfully render Loading', () => {
        const wrapper = shallow(<UserInformation {...baseProps} isLoading={false} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('successfully renderRow for table', () => {
        const wrapper = shallow(<UserInformation {...baseProps} />);

        expect(
            wrapper.instance().renderRow(
                new RepoRecord({
                    name: 'test',
                    description: 'test',
                    issues: null,
                    isPending: true,
                }),
                0,
            ),
        ).toMatchSnapshot();
    });

    test('successfully render Repo Table', () => {
        const wrapper = shallow(
            <UserInformation
                {...baseProps}
                repos={
                    new List([
                        new RepoRecord({
                            name: 'test',
                            description: 'test',
                            issues: null,
                            isPending: true,
                        }),
                    ])
                }
            />,
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
